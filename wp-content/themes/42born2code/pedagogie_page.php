<?php
/* Template Name: Pédagogie Page */
get_header() ?>
<div id="content">
<?php the_post() //cf. codex the_post() ?>
	<div class="entry">
		<h2 class="page-title"><?php the_title() ?></h2>
		<div class="entry-content">
		<?php
		if ( has_post_thumbnail() ){echo "PHOTO<BR>"; the_post_thumbnail();}

		else
		{
			echo "VIDEO<BR>";
		?>
		<iframe frameborder="0" width="480" height="270" src="http://www.dailymotion.com/embed/video/<?php echo get_post_meta($post->ID, 'id_dailymotion', true);?>" allowfullscreen></iframe><br />
		<?php
	}
		 the_content() //cf. codex the_content() ?>
		<?php echo get_post_meta($post->ID, 'Description de l\'image', true) ; ?>
		<?php wp_link_pages('before=<div id="page-links">&after=</div>'); ?>
		</div>
	</div><!-- entry -->
<?php if ( get_post_custom_values('comments') ) comments_template() ?>
</div><!-- #content -->

<?php if(is_page('Le programme')) get_sidebar();?>
<?php if(is_page('L\'équipe et les moyens')) include(TEMPLATEPATH."/team42.php");?>
<?php get_footer() ?>

