<?php get_header() ?>
<div id="content">
<?php the_post() //cf. codex the_post() ?>
	<div class="entry">
		<h2 class="page-title"><?php the_title() ?></h2>
		<div class="entry-content">
		<?php 
		 if ( has_post_thumbnail() ) the_post_thumbnail();
			echo get_post_meta($post->ID, 'id_dailymotion', true);
		 the_content() //cf. codex the_content() ?>
		<?php wp_link_pages('before=<div id="page-links">&after=</div>'); ?>
		<?php echo get_post_meta($post->ID, 'Description de l\'image', true) ; ?>
		</div>
	</div><!-- entry -->
<?php if ( get_post_custom_values('comments') ) comments_template() ?>
</div><!-- #content -->

<?php endif; ?>
<?php get_sidebar() ?>
<?php get_footer() ?>