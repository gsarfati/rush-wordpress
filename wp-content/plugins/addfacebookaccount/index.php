<?php
/*
		Plugin Name: Add Your Info s Account
		Description: Add Your Info s Account
		License: GPL
		Author: gsarfati
		Version: 1.0
*/



class AddInfo
{
    public function show_info($user)
    {
    	if(is_author('1'))
    	{	
    		echo esc_attr( get_the_author_meta('user_mood', $user->ID ) ).'<br>';
  		echo '<img src="https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-frc3/t31.0-8/1271084_10152203108461729_809245696_o.png?dl=1" width="25px" height="25px"/>';
		    	echo esc_attr( get_the_author_meta('facebook', $user->ID ) ).'<br>';
		echo '<img src="http://droidsoft.fr/wordpress/wp-content/uploads/2014/01/com-twitter-android.png" width="25px" height="25px"/>';
		        echo esc_attr( get_the_author_meta('twitter', $user->ID ) ).'<br>';
		echo '<img src="http://droidsoft.fr/wordpress/wp-content/uploads/2014/04/com-dailymotion-dailymotion.png" width="25px" height="25px"/>';
		    	echo esc_attr( get_the_author_meta('dailymotion', $user->ID ) ).'<br>';
    	}
    }
    public function fb_add_custom_user_profile_fields($user)
    {
    	$mood = esc_attr(get_the_author_meta('usr_mood', $user->ID));
    	?>
    	
    	Humeur: <select name="user_mood">
                <option <?php if ($mood == "") echo selected; ?>></option>
                <option <?php if ($mood == "Joyeux") echo selected; ?>> - HEC TIME</option>
                <option <?php if ($mood == "Enervé") echo selected; ?>> - Enervé</option>
                <option <?php if ($mood == "Envie de partir a HEC") echo selected; ?>> - Boulot </option>
                <option <?php if ($mood == "Fatigué") echo selected; ?>> - Fatigué</option>
                <option <?php if ($mood == "Somnolent") echo selected; ?>> - Cordiale</option>
                <option <?php if ($mood == "Revigoré") echo selected; ?>> - Happy</option>
                </select><br />
       <?php
       echo '<br><br><h3>'._e('Votre page :', 'your_textdomain').'</h3>';
 		echo '<table class="form-table"><tr><th><label for="facebook">'._e('Facebook', 'your_textdomain').'</label>';
		echo '<img src="https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-frc3/t31.0-8/1271084_10152203108461729_809245696_o.png?dl=1" width="25px" height="25px"/>';
		echo '</th><td><input type="text" name="facebook" id="facebook" value="'.esc_attr( get_the_author_meta('facebook', $user->ID ) ).'"class="regular-text" /><br />';
		echo '<span class="description">';
		echo _e('Vous pouvez ajouter votre page facebook', 'your_textdomain').'</span>';
		echo '</td></tr></table>';

		echo '<h3>'._e('Votre page :', 'your_textdomain').'</h3>';
 		echo '<table class="form-table"><tr><th><label for="twitter">'._e('Twitter', 'your_textdomain').'</label>';
		echo '<img src="http://droidsoft.fr/wordpress/wp-content/uploads/2014/01/com-twitter-android.png" width="25px" height="25px"/>';
		echo '</th><td><input type="text" name="twitter" id="twitter" value="'.esc_attr( get_the_author_meta('twitter', $user->ID ) ).'"class="regular-text" /><br />';
		echo '<span class="description">';
		echo _e('Vous pouvez ajouter votre page twitter', 'your_textdomain').'</span>';
		echo '</td></tr></table>';

		echo '<h3>'._e('Votre Video :', 'your_textdomain').'</h3>';
 		echo '<table class="form-table"><tr><th><label for="dailymotion">'._e('Dailymotion', 'your_textdomain').'</label>';
		echo '<img src="http://droidsoft.fr/wordpress/wp-content/uploads/2014/04/com-dailymotion-dailymotion.png" width="25px" height="25px"/>';
		echo '</th><td><input type="text" name="dailymotion" id="dailymotion" value="'.esc_attr( get_the_author_meta('dailymotion', $user->ID ) ).'"class="regular-text" /><br />';
		echo '<span class="description">';
		echo _e('Vous pouvez ajouter une Video dailymotion', 'your_textdomain').'</span>';
		echo '</td></tr></table>';
    }
    public function fb_save_custom_user_profile_fields($user_id) 
	{ 
		if(!current_user_can('edit_user', $user_id))
			return FALSE;
		update_usermeta($user_id, 'facebook', $_POST['facebook']);
		update_usermeta($user_id, 'twitter', $_POST['twitter']);
		update_usermeta($user_id, 'dailymotion', $_POST['dailymotion']);
		update_usermeta($user_id, 'user_mood', $_POST['user_mood']);
	}
}

add_action('show_user_profile', 'AddInfo::fb_add_custom_user_profile_fields');
add_action('edit_user_profile', 'AddInfo::fb_add_custom_user_profile_fields');

add_action('personal_options_update', 'AddInfo::fb_save_custom_user_profile_fields');
add_action('edit_user_profile_update', 'AddInfo::fb_save_custom_user_profile_fields');

register_sidebar_widget('My Widget', array('AddInfo', 'show_info')); 
